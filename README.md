# SimpleTube
Simpletube is a NodeJS youtube client without ads or client-sided javascript.
It is currently being completely rewritten to use youtube.js, so some functions may be partially broken. Thank you for your patience!

# To-Do
## Player Goals
- [x] Make a basic video player
- [x] Make it *stylish*
- [ ] Add channel info
- [ ] Add captions
- [ ] Show channel on watch page
- [ ] Add live support
- [ ] Add playlist support
## Search Goals
- [x] Add searching of videos
- [x] Add searching of channels
- [ ] Add searching of playlists
- [ ] Add searching of lives (Note: Searching of lives has been implemented before, but I want to implement it in the same update as actual stream support.)
- [ ] Add specific filter options
- [x] Add a screen for when a search fails
- [ ] Add mutiple search result pages
## Caching
- [x] "Cache" videos
- [ ] Add cache options
- [ ] Add options for sized based caching
## Channels
- [ ] Show Links
## Other
- [x] Add viewing of channels (Experimental)
- [ ] Make documentation
- [ ] Make links clickable by default