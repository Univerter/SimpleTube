const express = require("express"),
    path = require("path"),
    fs = require("fs"),
    ytjs = require('youtubei.js')

const PORT = process.env.PORT || 8080

var app = express()

var resources = path.join(__dirname, 'resources')
var staticpath = path.join(__dirname, 'static')

app.listen(PORT, () => {
    console.log("SimpleTube is now listening on port " + PORT)
})

app.use(express.static(staticpath))

var thumbCount = 0

// Proxy images through thumbor instances that alternate to prevent:
// 1. Direct communication with Youtube Services
// 2. Getting rate limited
// 3. Gathering too much data from one source
// More thumbor instances will be added

var thumborInstances = [ 
    "https://thumbor-production-0e82.up.railway.app/",
    "https://enormous-book-production.up.railway.app/",
    "https://unusual-back-production.up.railway.app/",
    "https://axiomatic-hair-production.up.railway.app/"
]

function getThumbor() {
    thumbCount += 1
    return thumborInstances[thumbCount % thumborInstances.length] + "unsafe"
}

function searchResultToHTML(results) {
    var videosHTML = "<h2>Videos:<br></h2>"
    var channelsHTML = "<h2>Channels:<br></h2>"
    var addedHTML = ""
    for (let index = 0; index < results.length; index++) {
        const result = results[index].content || results[index];
        try {
            if (result && result.type == "Video" && result.published && result.duration.text != "N/A" && result.thumbnails && result.author.thumbnails) {
                if (!result.description_snippet) {
                    if (result.snippets) {
                        result.description_snippet = result.snippets[0].text.runs[0]
                    } else {
                        result.description_snippet = {text: ''}
                    }
                }
                videosHTML += `
                <div class="col-xxl-4 col-sm-6 resultContainer">
                    <div class="videoResult container-fluid row">
                        <div class="col-lg-6 thumbparent">
                            <a class="videoLink" href="/watch?v=${result.id}">
                                <img class="thumbnail" src="${getThumbor()}/${result.thumbnails[0].url}">
                                <p style="display: block; text-align: left;">${result.duration.text}</p>
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a class="videoLink" href="/watch?v=${result.id}">
                                <p style="font-size: 1.25rem;">${result.title.text || "No Title Found"}</p>
                                <p class="resultDescription">${(result.description_snippet).text}</p>
                                <p style="display: block;">${result.published.text}</p>
                            </a>
                        </div>
                        <div style="display: inline-block; width: 100%;">
                            <a style="color: white; margin: 10px; display: inline-block;" href="/channel?q=${result.author.id}">
                            <img src="${getThumbor()}/${result.author.thumbnails[0].url}" class="minipfp">
                            ${result.author.name}
                            </a>
                        </div>
                    </div>
                </div>
                `
            } else if (result.type == "Channel" && result.author.thumbnails[0]) {
                channelsHTML += `
                <div class="col-xxl-4 col-sm-6 resultContainer">
                    <div class="videoResult container-fluid row">
                        <div class="col-lg-6 thumbparent">
                            <a class="videoLink" href="/channel?v=${result.id}">
                                <img class="pfp" src="${getThumbor()}/https:${result.author.thumbnails[0].url}">
                                <p style="display: block; text-align: left;">${result.video_count.text}</p>
                            </a>
                        </div>
                        <div class="col-lg-6">
                            <a class="videoLink" href="/channel?v=${result.id}">
                                <p style="font-size: 1.25rem;">${result.author.name}<br>
                                <span class="note">${result.subscriber_count.text}</span></p>
                                <p class="resultDescription">${(result.description_snippet).text}</p>
                            </a>
                        </div>
                    </div>
                </div>
                `
            } else {
                console.log(result.type)
            }    
        } catch (error) {
            console.error(error)
            console.log(result)
        }
    }

    if (channelsHTML.length > 30) {
        addedHTML += channelsHTML
    }
    if (videosHTML.length > 30) {
        addedHTML += videosHTML
    }

    addedHTML += "<style>#hideOnLoad { display: none; }</style>"

    return addedHTML
}

app.get("/", async (req, res) => {
    var innerTube = await ytjs.Innertube.create()

    res.setHeader("Content-Type", "text/html")
    res.setHeader("X-Accel-Buffering", "no")

    var html = fs.readFileSync(path.join(resources, 'mainPage.html')).toString()
    
    res.write(html.substring(0, html.indexOf("{RESULTS}")))

    var results = (await innerTube.getHomeFeed()).contents.contents

    var addedHTML = searchResultToHTML(results)

    res.write(addedHTML + html.substring(html.indexOf("{RESULTS}") + 9), () => {res.end()})
})

app.get("/search", async (req, res) => {
    var search = req.query.q
    var innerTube = await ytjs.Innertube.create()

    res.setHeader("Content-Type", "text/html")
    res.setHeader("X-Accel-Buffering", "no")

    var html = fs.readFileSync(path.join(resources, 'searchPage.html')).toString()
    
    html = html.replaceAll("{SEARCH}", search)

    res.write(html.substring(0, html.indexOf("{RESULTS}")))

    var results = (await innerTube.search(search, {type: "all"}))

    var addedHTML = searchResultToHTML(results.results)

    res.write(addedHTML + html.substring(html.indexOf("{RESULTS}") + 9), () => {res.end()})
})

// process.on('uncaughtException', (err, origin) => {
//     fs.writeSync(
//         process.stderr.fd,
//         `Caught exception: ${err}\n` +
//         `Exception origin: ${origin}`,
//     );
// });